﻿using UnityEngine;
using System.Collections;

public class Gem : MonoBehaviour
{
    
    // Use this for initialization
    private float speed;
    public float acceleration;

    void Start()
    {
        speed = 0.0f;
    }
    
    // Update is called once per frame
    void Update()
    {
        if (transform.position.y > -0.35f) // Временно, для демонстрации.
        {
            transform.position = new Vector3(transform.position.x, transform.position.y + speed);
        }
    }
    
    void OnMouseDown()
    {
        speed += acceleration;
        /*GameObject crop = GameObject.Find("gameManager");
        CropSprite script = (CropSprite) crop.GetComponent(typeof(CropSprite));
        script.CropSprite1();*/
    }
}
