﻿using UnityEngine;
using System.Collections;

public class Cell : MonoBehaviour
{

    public GameObject spriteToCrop;
    public int pixelsToUnits; // It's PixelsToUnits of sprite which would be cropped
    // Use this for initialization
    void Start()
    {
    
    }
    
    // Update is called once per frame
    void Update()
    {
    
    }

    void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0))
        {
            CropSpriteOver();
        }
        if(Input.GetMouseButtonDown(1))
        {
            CropSpriteFront();
        }
    }

    private void CropSpriteOver()
    {
        Vector3 position = new Vector3();
        position.x = this.transform.position.x;
        position.y = this.transform.position.y + 1;
        CropSprite(position);
    }

    private void CropSpriteFront()
    {
        Vector3 position = new Vector3();
        position.x = this.transform.position.x;
        position.y = this.transform.position.y;
        CropSprite(position);
    }

    private void CropSprite(Vector3 position)
    {
        //      Calculate topLeftPoint and bottomRightPoint of drawn rectangle
        Vector3 topLeftPoint  = new Vector3();
        Vector3 bottomRightPoint  = new Vector3();
        topLeftPoint.x = position.x - this.transform.localScale.x / 2;
        topLeftPoint.y = position.y + this.transform.localScale.y / 2;
        bottomRightPoint.x = position.x + this.transform.localScale.x / 2;
        bottomRightPoint.y = position.y - this.transform.localScale.y / 2;
        
        SpriteRenderer spriteRenderer = spriteToCrop.GetComponent<SpriteRenderer>();
        Sprite spriteToCropSprite = spriteRenderer.sprite;
        Texture2D spriteTexture = spriteToCropSprite.texture;
        Rect spriteRect = spriteToCrop.GetComponent<SpriteRenderer>().sprite.textureRect;
        
        GameObject croppedSpriteObj = new GameObject("croppedBackGround");
        Rect croppedSpriteRect = spriteRect;
        croppedSpriteRect.width = (Mathf.Abs(bottomRightPoint.x - topLeftPoint.x) * pixelsToUnits) * (1 / spriteToCrop.transform.localScale.x);
        croppedSpriteRect.x = (Mathf.Abs(topLeftPoint.x - (spriteRenderer.bounds.center.x - spriteRenderer.bounds.size.x / 2)) * pixelsToUnits) * (1 / spriteToCrop.transform.localScale.x);
        croppedSpriteRect.height = (Mathf.Abs(bottomRightPoint.y - topLeftPoint.y) * pixelsToUnits) * (1 / spriteToCrop.transform.localScale.y);
        croppedSpriteRect.y = ((topLeftPoint.y - (spriteRenderer.bounds.center.y - spriteRenderer.bounds.size.y / 2)) * (1 / spriteToCrop.transform.localScale.y)) * pixelsToUnits - croppedSpriteRect.height;//*(spriteToCrop.transform.localScale.y);
        Sprite croppedSprite = Sprite.Create(spriteTexture, croppedSpriteRect, new Vector2(0, 1), pixelsToUnits);
        SpriteRenderer cropSpriteRenderer = croppedSpriteObj.AddComponent<SpriteRenderer>();    
        cropSpriteRenderer.sprite = croppedSprite;
        topLeftPoint.z = -1;
        croppedSpriteObj.transform.position = topLeftPoint;
        croppedSpriteObj.transform.parent = spriteToCrop.transform.parent;
        croppedSpriteObj.transform.localScale = spriteToCrop.transform.localScale;
        croppedSpriteObj.renderer.sortingOrder = 4;
        croppedSpriteObj.transform.parent = this.transform;
        //Destroy(spriteToCrop);
    }
}
